<?php


/*

[ACTIVITY]

• Create the necessary getters and setters for the floors and address properties of Condominium.
• Change your code so that a building's floor and address property cannot be changed after creation.
• Finally, change your code in such a way that will let both the classes use the getters and setters that you created

*/

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

class Building {
    private $name;
    private $floors;
    private $address;

    // Constructor to initialize the Building object with a name, number of floors, and address.
    public function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    // Getter method to retrieve the name, floors, and address of the building.
    public function getName() {
        return $this->name;
    }

    public function getFloors() {
        return $this->floors;
    }

    public function getAddress() {
        return $this->address;
    }

    // Method to change the name of the building.
    public function changeName($newName) {
        $this->name = $newName;
    }
}

// The Condominium class is marked as 'final' to prevent further inheritance.
final class Condominium extends Building {

}


?>
